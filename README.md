This repo contains the code that powers the [dbt run results auditor](https://dbt-run-results-auditor.onrender.com/)
that was first introduced on [uptime's engineering blog](https://engineering.uptime.ac/).

# Run locally

- create a new venv and `pip install -r requirements.txt`
- then run `streamlit run app.py`
