import streamlit as st
import pandas as pd
import plotly.express as px
from random import randint
import base64
from PIL import Image
import json


sample_data_set_path = './public/sample_results/run_results.json'


@st.cache
def load_data(f):
    d = json.load(f)
    results, total_threads = format_results(d['results'])
    return (
        d['metadata'],
        results,
        total_threads,
        d['elapsed_time'],
        d['args']
    )


@st.cache
def format_results(r):
    thread_ids = []
    for i in range(len(r)):
        r[i]['started_at'] = r[i]['timing'][0]['started_at']
        r[i]['completed_at'] = r[i]['timing'][-1]['completed_at']
        r[i]['execution_time'] = round(r[i]['execution_time'], 2)

        if r[i]['thread_id'] not in thread_ids:
            thread_ids.append(r[i]['thread_id'])

        total_threads = len(thread_ids)

    return r, total_threads

# this helps us color mapping to threads
@st.cache
def build_color_mapping():
    c = dict()
    for i in range(300):
        c[f"Thread-{i}"] = '#%06X' % randint(i, 0xFFFFFF)
    return c


def get_state():
    if 'file' not in st.session_state:
        st.session_state.file = None

    if 'picked_file' not in st.session_state:
        st.session_state.picked_file = None
    elif st.session_state.picked_file != None:
        st.session_state.file = st.session_state.picked_file

    return st.session_state


def set_state(key, value):
    st.session_state[key] = value
    return get_state()


color_mapping = build_color_mapping()
st.session_state.picked_file = st.sidebar.file_uploader(
    'select a run_results.json file'
)

if get_state().file is None:
    left, right = st.columns([3, 3])

    with left:
        '''
        # dbt run results auditer
        Identify **slow models** and **bottlenecks**.
        ####
        '''
        st.button(
            'Try it out with dummy data!',
            on_click= lambda: set_state('file', open(sample_data_set_path))
        )

    with right:
        teaser = Image.open('./public/media/teaser.png')
        st.image(teaser, caption="")

    '''
    Please select the output of your dbt job (ie. a `/target/run_results.json`
    file) using the sidebar.

    ----
    '''

    left, right = st.columns([2, 1])
    with left:
        file_ = open("./public/media/uptime-light-speed.gif", "rb")
        contents = file_.read()
        data_url = base64.b64encode(contents).decode("utf-8")
        file_.close()
        st.markdown(
            f'<img src="data:image/gif;base64,{data_url}" alt="uptime">',
            unsafe_allow_html=True,
        )

    # a nice looking gif to give it some pep
    with right:
        '''
        Hi there 👋 This was built by the data team at [uptime.ac](https://uptime.ac)
        and was featured on [our tech blog](https://engineer.uptime.ac).

        We love using data to make sure you don't get stuck in the elevator,
        please feel free to [reach out](https://twitter.com/uptime_ac)!
    '''

else:
    metadata, results, total_threads, elapsed, args = load_data(
        get_state().file
    )
    st.title(f"Optimizing dbt {args['which']}")
    st.text(
        f"""
A {args['which']} of {len(results)} models that took {round(elapsed, 2)} \
seconds spread over {total_threads} thread(s).""")
    r = pd.DataFrame(results)

    # display raw data
    if st.checkbox('Show raw data'):
        st.subheader('Raw data')
        st.write(r)

    # treemap of most painful models
    '''
  ## Slowest threads and models
  The bigger the area, the more time the model takes to run.
  This helps identifying the thread that takes the longest time to run, and
  why.
  '''
    treemap = px.treemap(
        r, path=[px.Constant("all threads"), 'thread_id', 'unique_id'],
        values='execution_time', color='thread_id',
        color_discrete_map=color_mapping,
        hover_data=r[['message']]
    )
    st.plotly_chart(treemap, use_container_width=True)

    # gantt chart - bottle
    '''
  ## Identifying bottenecks
  This Gantt chart shows the order in which models are being executed, and
  display times where a thread is left with no other tasks to run. This helps
  understanding if some models are bottlenecks.

  Mouse over to get the model names and more details.
  '''
    gantt = px.timeline(r, x_start="started_at", x_end="completed_at",
                        y="thread_id", color="thread_id",
                        color_discrete_map=color_mapping,
                        hover_name="unique_id",
                        hover_data=r[['execution_time', 'message']])
    gantt.update_yaxes(title=None, autorange='reversed')
    gantt.update_layout(showlegend=False)
    st.plotly_chart(gantt, use_container_width=True)

# distribution
# st.subheader('Number of models by exec times')
# st.text('''
# A quick look at the dist
# ''')
# r['exec_seconds'] = np.ceil(r.execution_time)
# distribution = r
# distribution = distribution.
#   groupby(['exec_seconds']).count()['status'].reset_index()
# distribution.rename(columns={"status": "total_models"})
# distribution.columns = ['exec_seconds', 'total_models']
# bars = alt.Chart(distribution).mark_bar().encode(
#   x='exec_seconds:Q',
#   y='total_models:O'
# )
# st.altair_chart(bars, use_container_width=True)
